import csv
from string import Template
students = csv.DictReader(open('dbtest.csv'))
tdir = './templates/'

separators = ('\clearpage')
counter = 1

print(open(tdir + 'preamble.tex').read())
print('\\begin{document}')

for student in students:
    template = Template(open(tdir + 'body.tex').read().rstrip('\n'))
    print(template.substitute(student))
    counter +=1

print('\end{document}')E